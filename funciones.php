<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once('Config.php');

function cabecera() {
   echo "<h1>" . TITULO . "</h1>\n";
   echo "<h2>" . GRUPO . "  </h2><hr/>\n";
}

function pie() {
   echo "<hr/><pre>" . EMPRESA . " " . GRUPO . " ";
   echo CURSO . " " . FECHA . "</pre>\n";
}

function inicio() {
   echo '<align="right"><a href = "index.php" target="inicio" >Inicio</a> </align>';
}

function grupo() {

   echo "<p>Datos del grupo:</p>";

   echo "<table border='1'>";
   echo "<tr>";
   echo "<th>Alumno#</th>";
   echo "<th>Nombre</th>";
   echo "<th>Tema</th>";
   echo "<th>Entregado</th>";
   echo "<th>Bitbucket</th>";
   echo "<th>Heroku</th>";
   echo "</tr>";

   echo "<tr>";
   echo "<td>Profesor</td>";
   echo "<td>Paco Aldarias</td>";
   echo "<td> Tema2 </td>";
   echo "<td> 3/10/2017</td>";
   echo "<td>";
   echo '<a href="https://bitbucket.org/pacoaldarias/pacoaldariasdwst2e1" target="tema2p">' . 'Bitbucket2</a>';
   echo "</td>";
   echo "<td>";
   echo '<a href ="https://pacoaldariasdwst2e1.herokuapp.com" target="tema2a1">' . 'UrlHeroku2</a>';
   echo "</td>";
   echo "</tr>";

   echo "<tr>";
   echo "<td>Alumno1</td>";
   echo "<td>Nombre Alumno1</td>";
   echo "<td> Tema2 </td>";
   echo "<td></td>";
   echo "<td>";
   echo '<a href="https://bitbucket.org/pacoaldarias/xdwst2e1" target="tema2b1">' . 'Bitbucket2</a>';
   echo "</td>";
   echo "<td>";
   echo '<a href ="https://xdwst2e1.herokuapp.com/" target="tema2h1">' . 'UrlHeroku2</a>';
   echo "</td>";
   echo "</tr>";

   echo "<tr>";
   echo "<td>Alumno2</td>";
   echo "<td>Nombre Alumno2</td>";
   echo "<td> Tema2 </td>";
   echo "<td></td>";
   echo "<td>";
   echo '<a href="https://bitbucket.org/pacoaldarias/xdwst2e1" target="tema2b2">' . 'Bitbucket2</a>';
   echo "</td>";
   echo "<td>";
   echo '<a href ="https://xdwst2e1.herokuapp.com/" target="tema2h2">' . 'UrlHeroku2</a>';
   echo "</td>";
   echo "</tr>";

   echo "<tr>";
   echo "<td>Alumno3</td>";
   echo "<td>Sergio Esteve Gascó</td>";
   echo "<td> Tema2 </td>";
   echo "<td>17/10/2017</td>";
   echo "<td>";
   echo '<a href="https://bitbucket.org/sesteve/sestevedwst2e1/" target="tema2b3">' . 'Bitbucket2</a>';
   echo "</td>";

   echo "<td>";
   echo '<a href ="https://sestevedwst2e1.herokuapp.com/" target="tema2h3">' . 'UrlHeroku2</a>';
   echo "</td>";


   echo "</tr>";
   echo "</table>";
}

function docu() {
   echo "<p>Documentación por tema:</p>";
   echo "<ul>";
   echo '<li><a href="https://docs.google.com/document/d/1UkUUUuSEwYrY1ZCLRphlMEevXBY8ONYkMK7IA-fYA34/edit?usp=sharing" target="_blank">Tema 2. PHP</a> </li>';
   echo "</ul>";
}
?>

